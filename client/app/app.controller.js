
(function () {
    "use strict";
    angular.module("MyApp").controller("MyAppCtrl", MyAppCtrl);

    MyAppCtrl.$inject = ["$http", "$log"];

    function MyAppCtrl($http) {
        var self = this; // vm
        self.completedRegistration = false;
        self.submissionError = false;
        self.countries = [];
        self.user = {
            name: "",
            email: "",
            contact: "",
            password: "",
            password2: "",
            gender: "",
            dob: "",
            address: "",
            country: ""
        };
        self.progress=10;

        self.initForm = function () {
            self.completedRegistration = false;
            self.submissionError = false;

            $http.get("/countries")
            .then(function (result) {
                // console.log(result);
                self.countries = result.data;
            }).catch(function (e) {
                console.log(e);
            });
        };

        self.initForm();

        self.isAtLeast18 = function () {
            if (self.user.dob == "" || typeof(self.user.dob) == "undefined") {
                return false;
            }

            return _calculateAge(self.user.dob) >= 18;
        }

        self.isPasswordConfirmed = function () {
            if (self.user.password == "" || self.user.password2 == "") {
                return false;
            }

            return self.user.password == self.user.password2;
        }

        // function to update progress bar
        self.setProgress = function(v) {
            // progress is only incremented
            if(v > self.progress) {
                self.progress = v;
            }
        }

        self.submitForm = function () {

            $http.post("/register", self.user)
                .then(function (result) {
                    // console.log(result);
                    if(result.status == 200) {
                        self.completedRegistration = true;
                    } else {
                        self.submissionError = true;
                    }
                }).catch(function (e) {
                    console.log(e);
                    self.submissionError = true;
                });
        }
    }

    function _calculateAge(birthday) { // birthday is a date
        if(typeof(birthday) == "undefined") {
            return 0;
        }

        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs); // miliseconds from epoch
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }

})();