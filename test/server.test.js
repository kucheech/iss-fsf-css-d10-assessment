"use strict";
const request = require('supertest');
const app = require('../server/app');

/* test root path */
describe('Test the root path', function() {
    test('It should response the GET method', function() {
        return request(app).get("/").then(function(response) {
            expect(response.statusCode).toBe(200);
        })
    });
});

/** test no end point found */
describe('Test no end point found', function() {
    test('It return page not found in response', function() {
        return request(app).get("/sdffdf").then(function(response) {
            expect(response.statusCode).toBe(404);
        })
    });
});

/** test /countries endpoint */
describe('Test /countries endpoint', function() {
    test('It should return an array', function() {
        return request(app).get("/countries").then(function(response) {
            expect(response.statusCode).toBe(200);
            expect(response.type).toBe("application/json");
            var array = response.body;
            // console.log(JSON.stringify(response.body));
            //208 countries
            expect(response.body.length).toBe(208);
        })
    });
});